FROM debian:latest
RUN groupadd -g 106 nsd && \
    groupadd -g 107 ssl-cert && \
    useradd  -g 106 -u 104 nsd && \
    useradd  -g 107 -u 401 acme && \
    apt-get update && \
    apt-get -y install openssl git nsd ldnsutils nano wget curl && \
    \
    cd /opt && \
    git clone https://gitlab.com/codedump2/zonemgr.git && \
    git clone https://github.com/acmesh-official/acme.sh.git && \
    \
    mkdir /etc/ssl/custom && \
    mkdir /home/acme

ENV PATH=$PATH:/opt/zonemgr:/opt/acme.sh

# VOLUME /etc/nsd
# VOLUME /etc/ssl/custom
