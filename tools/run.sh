#!/bin/bash

podman run -ti --rm \
       -v /var/storage/storage/config/nsd/etc-nsd:/etc/nsd:z \
       -v /var/storage/storage/config/ssl/rootshell:/etc/ssl/custom:z \
       -v /var/storage/storage/config/ssl/acme-home:/home/acme \
       acme \
       bash
